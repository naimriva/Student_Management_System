-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 24, 2017 at 07:41 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `student_management_system`
--
CREATE DATABASE IF NOT EXISTS `student_management_system` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `student_management_system`;

-- --------------------------------------------------------

--
-- Table structure for table `student_info`
--

CREATE TABLE IF NOT EXISTS `student_info` (
`id` int(11) NOT NULL,
  `student_id` varchar(20) NOT NULL,
  `student_name` varchar(100) NOT NULL,
  `fathers_name` varchar(50) NOT NULL,
  `mothers_name` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `email` varchar(120) NOT NULL,
  `contact_no` int(15) NOT NULL,
  `birthday` date NOT NULL,
  `nationality` varchar(50) NOT NULL,
  `religion` varchar(20) NOT NULL,
  `present_address` varchar(500) NOT NULL,
  `permanent_address` varchar(500) NOT NULL,
  `blood_group` varchar(10) NOT NULL,
  `student_class` varchar(12) NOT NULL,
  `student_group` varchar(20) NOT NULL,
  `student_section` varchar(5) NOT NULL,
  `admission_date` date NOT NULL,
  `file_name` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_info`
--

INSERT INTO `student_info` (`id`, `student_id`, `student_name`, `fathers_name`, `mothers_name`, `gender`, `email`, `contact_no`, `birthday`, `nationality`, `religion`, `present_address`, `permanent_address`, `blood_group`, `student_class`, `student_group`, `student_section`, `admission_date`, `file_name`) VALUES
(1, 'L-123456', 'Sahed Iqbal', 'Nurul Islam', 'Sultana Begum', 'Male', 'rubelwd@gmail.com', 1815816466, '2017-02-16', 'Bangladeshi(By birth)', 'Islam', 'Present Address', 'Permanent Address', 'A+', 'Class 7', '', 'B', '2017-02-17', ''),
(2, 'L-123453', 'Sahed Sazzad', 'Zafar chy', 'Samia Rahaman', 'Male', 'c.o1@bitm.org.bd', 1918768767, '2017-02-25', 'Bangladeshi(By birth)', 'Islam', 'Present address', 'Permanent Address', 'O+', 'Class 9', 'Science', 'D', '2017-02-10', 'img10.jpg'),
(3, 'L111098', 'Javed Karim', 'Abdul Karim', 'Amena Karim', 'Male', 'jabedkarim@gmail.com', 1918767856, '2017-02-07', 'Bangladeshi(By birth)', 'Islam', 'Chittagong, Bangladesh', 'Chittagong, Bangladesh', 'AB+', 'Class 8', '', 'C', '2017-02-16', 'img7.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_academic_info`
--

CREATE TABLE IF NOT EXISTS `teacher_academic_info` (
`id` int(11) NOT NULL,
  `teacher_id` varchar(20) NOT NULL,
  `ssc` varchar(10) NOT NULL,
  `ssc_institution` varchar(100) NOT NULL,
  `ssc_board` varchar(50) NOT NULL,
  `ssc_gpa` float NOT NULL,
  `ssc_passing_year` date NOT NULL,
  `hsc` varchar(20) NOT NULL,
  `hsc_institution` varchar(100) NOT NULL,
  `hsc_board` varchar(50) NOT NULL,
  `hsc_gpa` float NOT NULL,
  `hsc_passing_year` date NOT NULL,
  `honours` varchar(20) NOT NULL,
  `honours_institution` varchar(100) NOT NULL,
  `honours_board` varchar(50) NOT NULL,
  `honours_gpa` float NOT NULL,
  `honours_passing_year` date NOT NULL,
  `masters` varchar(20) NOT NULL,
  `master_institution` varchar(100) NOT NULL,
  `master_board` varchar(50) NOT NULL,
  `master_gpa` float NOT NULL,
  `master_passing_year` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_academic_info`
--

INSERT INTO `teacher_academic_info` (`id`, `teacher_id`, `ssc`, `ssc_institution`, `ssc_board`, `ssc_gpa`, `ssc_passing_year`, `hsc`, `hsc_institution`, `hsc_board`, `hsc_gpa`, `hsc_passing_year`, `honours`, `honours_institution`, `honours_board`, `honours_gpa`, `honours_passing_year`, `masters`, `master_institution`, `master_board`, `master_gpa`, `master_passing_year`) VALUES
(1, 'T001', 'SSC', 'Dewanpur S.K. Sen High School', 'Chittagong', 5, '2017-02-08', 'HSC', 'Imam Gazzali University College', 'Chittagong', 4.98, '2017-02-28', 'Honours', 'International Islamic University Chittagong', 'International Islamic University Chittagong', 4, '2017-02-10', 'Masters', 'International Islamic University Chittagong', 'International Islamic University Chittagong', 3.45, '2017-02-26'),
(2, 'T002', 'SSC', 'Chittagong Govt. High', 'Chiitagong', 5, '2017-02-07', 'HSC', 'Chittagong College', 'Chiitagong', 5, '2017-02-17', 'Honours', 'Chittagong University', 'Chittagong University', 4, '2017-02-20', 'Masters', 'Chittagong University', 'Chittagong University', 4, '2017-02-25'),
(3, 'T003', 'SSC', 'Dhaka Zilla School', 'Dhaka Board', 5, '2017-02-14', 'HSC', 'Dhaka College', 'Dhaka Board', 4.98, '2017-02-18', 'Honours', 'Dhaka University', 'Dhaka University', 4, '2017-02-23', 'Masters', 'Dhaka University', 'Dhaka University', 3.45, '2017-02-07');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_info`
--

CREATE TABLE IF NOT EXISTS `teacher_info` (
`id` int(11) NOT NULL,
  `teacher_id` varchar(20) NOT NULL,
  `teacher_name` varchar(50) NOT NULL,
  `fathers_name` varchar(50) NOT NULL,
  `mothers_name` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `email` varchar(120) NOT NULL,
  `contact_no` int(15) NOT NULL,
  `birthday` date NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `religion` varchar(20) NOT NULL,
  `present_address` varchar(500) NOT NULL,
  `permanent_address` varchar(500) NOT NULL,
  `blood_group` varchar(10) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `joining_date` date NOT NULL,
  `subject` varchar(50) NOT NULL,
  `nid` int(20) NOT NULL,
  `reference` varchar(200) NOT NULL,
  `file_name` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_info`
--

INSERT INTO `teacher_info` (`id`, `teacher_id`, `teacher_name`, `fathers_name`, `mothers_name`, `gender`, `email`, `contact_no`, `birthday`, `nationality`, `religion`, `present_address`, `permanent_address`, `blood_group`, `designation`, `joining_date`, `subject`, `nid`, `reference`, `file_name`) VALUES
(1, 'T001', 'Rafiqul Islam', 'Habibur Rahaman', 'Sahanaj Akther', 'Male', 'rafiqulislam@yahoo.com', 1927814556, '1990-01-08', 'Bangladeshi', 'Islam', 'Muradpur 34/R/A ,Chittagong', 'Muradpur 34/R/A ,Chittagong', 'A+', 'Class Teacher', '2017-02-24', 'Bangla', 2147483647, 'Abdul Kalam,Senior Teacher, Bangla', '.jpg'),
(2, 'T002', 'Jabed Karim', 'Mahmudul Hasan', 'Sumaya Akter', 'Male', 'jabed.ctg.karim@gmail.com', 1816785645, '2017-02-01', 'Bangladeshi', 'Islam', 'Hathazari,Chittagong', 'Hathazari,Chittagong', 'O+', 'Class Teacher', '2017-02-25', 'English', 2147483647, 'Ashraful Kabir', '3386_407428169321566_822128144_n.jpg'),
(3, 'T003', 'Ibrahim Khondokar', 'Shimul pathowary', 'Joynab Begum', 'Male', 'ms24hossain@gmail.com', 1918181818, '2017-02-28', 'Bangladeshi', 'Islam', 'Gazipur, Dhaka', 'Do', 'O-', 'Class Teacher', '2017-02-22', 'Mathematics', 2147483647, 'Kabir Uddin', '35897_358416550889395_255957341135317_1003925_209555493_n.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_upload`
--

CREATE TABLE IF NOT EXISTS `teacher_upload` (
`id` int(11) NOT NULL,
  `teacher_id` varchar(20) NOT NULL,
  `description` varchar(500) NOT NULL,
  `file_name` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_upload`
--

INSERT INTO `teacher_upload` (`id`, `teacher_id`, `description`, `file_name`) VALUES
(1, 'T001', 'Mark Sheet', 'admitest-notice-2015-2016.doc'),
(6, 'T002', 'Mark Sheet 2', '01simple (6).xlsx'),
(9, 'T003', 'Lecture', 'Basic Syntax, Variable.docx'),
(11, 'T002', 'New File', 'Zend_PHP_Certification_Guide.pdf'),
(12, 'T003', 'Mark Sheet', 'Book Title.pdf'),
(13, 'T001', 'Test File', '01simple (3).xlsx'),
(14, 'T001', 'Test File', '01simple (4) (1).xlsx'),
(15, 'T001', 'Mark Sheet 2', '01simple (2).xlsx');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `student_info`
--
ALTER TABLE `student_info`
 ADD PRIMARY KEY (`id`), ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `teacher_academic_info`
--
ALTER TABLE `teacher_academic_info`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_info`
--
ALTER TABLE `teacher_info`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_upload`
--
ALTER TABLE `teacher_upload`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `student_info`
--
ALTER TABLE `student_info`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `teacher_academic_info`
--
ALTER TABLE `teacher_academic_info`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `teacher_info`
--
ALTER TABLE `teacher_info`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `teacher_upload`
--
ALTER TABLE `teacher_upload`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
