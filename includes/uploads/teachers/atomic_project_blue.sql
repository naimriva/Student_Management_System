-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 23, 2017 at 04:28 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomic_project_blue`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthdate`
--

CREATE TABLE IF NOT EXISTS `birthdate` (
`id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `birthdate` date NOT NULL,
  `soft_deleted` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
`id` int(11) NOT NULL,
  `book_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_deleted`) VALUES
(4, 'Islamic History and Culture', 'Ali Arefin', 'Yes'),
(7, 'Ratri', 'Taslim', 'Yes'),
(11, 'Science Fiction', 'Humayon', 'No'),
(12, 'Baker', 'Ami', 'No'),
(13, 'Nari', 'Humayun', 'Yes'),
(14, 'Fariha', 'Farhana Taslim', 'No'),
(16, 'Baji', 'Alo', 'Yes'),
(17, 'Moyna', 'Jorina', 'No'),
(18, 'Nari', 'morzina', 'No'),
(19, 'Maa', 'Ali Asgor', 'Yes'),
(20, 'Ekush', 'Abu Jafor', 'No'),
(21, 'Amar Desh', 'Syed Muhammad Alam', 'No'),
(22, 'Islamic History and Culture', 'Ali Arefin', 'No'),
(23, 'World', 'Ali Akkas', 'Yes'),
(24, 'Rajni', 'Rsl', 'Yes'),
(25, 'Riazus Saleheen', 'Unknown', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
`id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `soft_deleted`) VALUES
(1, 'Muhammad Ali Asgor', 'muhammadasgor331@gmail.com', 'No'),
(2, 'Maruful Islam', 'miskat@gmail.com', 'No'),
(3, 'Miskat', 'miskat@gmail.com', 'No'),
(7, 'Arefin Siddiqa', 'muhammadasgor331@gmail.com', 'No'),
(9, 'Omar Faruk', 'omar@gmail.com', 'No'),
(10, 'Razib', 'razib@gmail.com', 'No'),
(11, 'Gazi', 'gazi@gmail.com', 'No'),
(12, 'Sakib', 'sakib@gmail.com', 'No'),
(13, 'Lucky', 'lucky@gmail.com', 'No'),
(14, 'Mozib', 'mozib@gmail.com', 'No'),
(15, 'Rakib', 'arefin@gmail.com', 'No'),
(16, 'Alam', 'alijaker@gmail.com', 'No'),
(17, 'Abu Raihan', 'amma@gmail.com', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
`id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE IF NOT EXISTS `hobby` (
`id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `hobby` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `name`, `hobby`, `soft_deleted`) VALUES
(1, 'Muhammad Ali Asgor', 'Gardening,Music,Sports', 'No'),
(4, 'Arif', 'Sports,Engineering,Fishing,Racing', 'No'),
(5, 'Ramjan Ali', 'Music,Sports,Engineering,Fishing', 'Yes'),
(6, 'Akbar', 'Engineering,Fishing,Racing', 'No'),
(7, 'Asraf', 'Engineering,Fishing,Racing', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE IF NOT EXISTS `organization` (
`id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `summary` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`id`, `name`, `summary`, `soft_deleted`) VALUES
(2, 'BGMCD', 'Fine org', 'No'),
(3, 'Health', 'Big', 'No'),
(4, 'Real State', 'Land', 'No'),
(6, 'Cupco', 'Fertilizer Company', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE IF NOT EXISTS `profile_picture` (
`id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `profile_picture` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthdate`
--
ALTER TABLE `birthdate`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization`
--
ALTER TABLE `organization`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthdate`
--
ALTER TABLE `birthdate`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `organization`
--
ALTER TABLE `organization`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
