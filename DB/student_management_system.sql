-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 15, 2017 at 05:00 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `student_management_system`
--
CREATE DATABASE IF NOT EXISTS `student_management_system` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `student_management_system`;

-- --------------------------------------------------------

--
-- Table structure for table `arts`
--

CREATE TABLE IF NOT EXISTS `arts` (
`id` int(11) NOT NULL,
  `student_id` varchar(10) NOT NULL,
  `geography` int(3) NOT NULL,
  `economics` int(3) NOT NULL,
  `civics` int(3) NOT NULL,
  `history` int(3) NOT NULL,
  `group` varchar(10) NOT NULL DEFAULT 'arts'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `arts`
--

INSERT INTO `arts` (`id`, `student_id`, `geography`, `economics`, `civics`, `history`, `group`) VALUES
(1, 'S-111003', 76, 67, 76, 76, 'arts');

-- --------------------------------------------------------

--
-- Table structure for table `commerce`
--

CREATE TABLE IF NOT EXISTS `commerce` (
`id` int(11) NOT NULL,
  `student_id` varchar(10) NOT NULL,
  `accounting` int(3) NOT NULL,
  `finance` int(3) NOT NULL,
  `entrepreneurship` int(3) NOT NULL,
  `generalScience` int(3) NOT NULL,
  `group` varchar(10) NOT NULL DEFAULT 'commerce'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `commerce`
--

INSERT INTO `commerce` (`id`, `student_id`, `accounting`, `finance`, `entrepreneurship`, `generalScience`, `group`) VALUES
(1, 'S-111004', 433, 34, 43, 34, 'commerce');

-- --------------------------------------------------------

--
-- Table structure for table `employee_info`
--

CREATE TABLE IF NOT EXISTS `employee_info` (
`id` int(11) NOT NULL,
  `employee_id` varchar(20) NOT NULL,
  `joining_date` date NOT NULL,
  `employee_name` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `birthday` date NOT NULL,
  `nationality` varchar(50) NOT NULL,
  `religion` varchar(20) NOT NULL,
  `blood_group` varchar(10) NOT NULL,
  `fathers_name` varchar(100) NOT NULL,
  `mothers_name` varchar(100) NOT NULL,
  `contact_no` int(20) NOT NULL,
  `present_village` varchar(100) NOT NULL,
  `present_thana` varchar(50) NOT NULL,
  `present_upazilla` varchar(50) NOT NULL,
  `present_district` varchar(50) NOT NULL,
  `present_division` varchar(50) NOT NULL,
  `present_country` varchar(20) NOT NULL,
  `permanent_village` varchar(100) NOT NULL,
  `permanent_thana` varchar(50) NOT NULL,
  `permanent_upazilla` varchar(50) NOT NULL,
  `permanent_district` varchar(50) NOT NULL,
  `permanent_division` varchar(50) NOT NULL,
  `permanent_country` varchar(20) NOT NULL,
  `file_name` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_info`
--

INSERT INTO `employee_info` (`id`, `employee_id`, `joining_date`, `employee_name`, `gender`, `birthday`, `nationality`, `religion`, `blood_group`, `fathers_name`, `mothers_name`, `contact_no`, `present_village`, `present_thana`, `present_upazilla`, `present_district`, `present_division`, `present_country`, `permanent_village`, `permanent_thana`, `permanent_upazilla`, `permanent_district`, `permanent_division`, `permanent_country`, `file_name`) VALUES
(1, 'E-111001', '0000-00-00', 'Abdur Rashid', 'Male', '2017-03-08', 'Bangladeshi', 'Islam', 'O-', 'Abdul Hakim', 'Sufia Begum', 2147483647, 'Gumdondi', 'Panchlaish', 'Panchlaish', 'Chittagong', 'Chittagong', 'Bangladesh', 'Gumdondi', 'Panchlaish', 'Panchlaish', 'Chittagong', 'Chittagong', 'Bangladesh', 'img2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `general`
--

CREATE TABLE IF NOT EXISTS `general` (
`id` int(11) NOT NULL,
  `exam_type` varchar(20) NOT NULL,
  `class` varchar(10) NOT NULL,
  `student_id` varchar(10) NOT NULL,
  `marksBangla1` int(3) NOT NULL,
  `marksBangla2` int(3) NOT NULL,
  `marksEnglish1` int(3) NOT NULL,
  `marksEnglish2` int(3) NOT NULL,
  `mathematics` int(3) NOT NULL,
  `bgs` int(3) NOT NULL,
  `science` int(3) NOT NULL,
  `religion` int(3) NOT NULL,
  `agriculture` int(3) NOT NULL,
  `artsandcrafts` int(3) NOT NULL,
  `ict` int(3) NOT NULL,
  `wle` int(3) NOT NULL,
  `physicalEducation` int(3) NOT NULL,
  `group` varchar(10) NOT NULL DEFAULT 'general'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general`
--

INSERT INTO `general` (`id`, `exam_type`, `class`, `student_id`, `marksBangla1`, `marksBangla2`, `marksEnglish1`, `marksEnglish2`, `mathematics`, `bgs`, `science`, `religion`, `agriculture`, `artsandcrafts`, `ict`, `wle`, `physicalEducation`, `group`) VALUES
(1, 'monthly', '6', 'S-111001', 56, 65, 65, 56, 65, 56, 56, 56, 56, 56, 56, 56, 56, 'general'),
(2, 'monthly', '9', 'S-111002', 76, 76, 76, 76, 67, 76, 76, 76, 76, 67, 76, 76, 76, 'general'),
(3, 'monthly', '9', 'S-111003', 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 76, 'general'),
(4, 'monthly', '9', 'S-111004', 34, 43, 43, 34, 43, 43, 43, 43, 43, 43, 43, 43, 43, 'general'),
(5, 'monthly', '7', 'S-111005', 34, 34, 34, 34, 34, 34, 34, 43, 43, 43, 43, 43, 34, 'general');

-- --------------------------------------------------------

--
-- Table structure for table `general_2`
--

CREATE TABLE IF NOT EXISTS `general_2` (
`id` int(11) NOT NULL,
  `exam_type` varchar(20) NOT NULL,
  `class` varchar(10) NOT NULL,
  `student_id` varchar(10) NOT NULL,
  `marksBangla1` int(3) NOT NULL,
  `bangla1PassMark` int(3) NOT NULL,
  `bangla1Written` int(3) NOT NULL,
  `bangla1Obj` int(3) NOT NULL,
  `bangla1Practical` int(3) NOT NULL,
  `bangla1Total` int(3) NOT NULL,
  `bangla1Highest` int(3) NOT NULL,
  `marksBangla2` int(3) NOT NULL,
  `bangla2PassMark` int(3) NOT NULL,
  `bangla2Written` int(3) NOT NULL,
  `bangla2Obj` int(3) NOT NULL,
  `bangla2Practical` int(3) NOT NULL,
  `bangla2Total` int(3) NOT NULL,
  `bangla2Highest` int(3) NOT NULL,
  `marksEnglish1` int(3) NOT NULL,
  `english1PassMark` int(3) NOT NULL,
  `english1Written` int(3) NOT NULL,
  `english1Obj` int(3) NOT NULL,
  `english1Practical` int(3) NOT NULL,
  `english1Total` int(3) NOT NULL,
  `english1Highest` int(3) NOT NULL,
  `marksEnglish2` int(3) NOT NULL,
  `english2PassMark` int(3) NOT NULL,
  `english2Written` int(3) NOT NULL,
  `english2Obj` int(3) NOT NULL,
  `english2Practical` int(3) NOT NULL,
  `english2Total` int(3) NOT NULL,
  `english2Highest` int(3) NOT NULL,
  `mathematics` int(3) NOT NULL,
  `mathematicsPassMark` int(3) NOT NULL,
  `mathematicsWritten` int(3) NOT NULL,
  `mathematicsObj` int(3) NOT NULL,
  `mathematicsPractical` int(3) NOT NULL,
  `mathematicsTotal` int(3) NOT NULL,
  `mathematicsHighest` int(3) NOT NULL,
  `bgs` int(3) NOT NULL,
  `bgsPassMark` int(3) NOT NULL,
  `bgsWritten` int(3) NOT NULL,
  `bgsObj` int(3) NOT NULL,
  `bgsPractical` int(3) NOT NULL,
  `bgsTotal` int(3) NOT NULL,
  `bgsHighest` int(3) NOT NULL,
  `science` int(3) NOT NULL,
  `sciencePassMark` int(3) NOT NULL,
  `scienceWritten` int(3) NOT NULL,
  `scienceObj` int(3) NOT NULL,
  `sciencePractical` int(3) NOT NULL,
  `scienceTotal` int(3) NOT NULL,
  `scienceHighest` int(3) NOT NULL,
  `religion` int(3) NOT NULL,
  `religionPassMark` int(3) NOT NULL,
  `religionWritten` int(3) NOT NULL,
  `religionObj` int(3) NOT NULL,
  `religionPractical` int(3) NOT NULL,
  `religionTotal` int(3) NOT NULL,
  `religionHighest` int(3) NOT NULL,
  `agriculture` int(3) NOT NULL,
  `agriculturePassMark` int(3) NOT NULL,
  `agricultureWritten` int(3) NOT NULL,
  `agricultureObj` int(3) NOT NULL,
  `group` varchar(10) NOT NULL DEFAULT 'general'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `highest_mark`
--

CREATE TABLE IF NOT EXISTS `highest_mark` (
`id` int(11) NOT NULL,
  `examType` varchar(20) NOT NULL,
  `class` varchar(20) NOT NULL,
  `bangla1` int(3) NOT NULL,
  `bangla2` int(3) NOT NULL,
  `english1` int(3) NOT NULL,
  `english2` int(3) NOT NULL,
  `mathematics` int(3) NOT NULL,
  `bgs` int(3) NOT NULL,
  `science` int(3) NOT NULL,
  `religion` int(3) NOT NULL,
  `agriculture` int(3) NOT NULL,
  `artsandcrafts` int(3) NOT NULL,
  `ict` int(3) NOT NULL,
  `wle` int(3) NOT NULL,
  `physicalEducation` int(3) NOT NULL,
  `physics` int(3) NOT NULL,
  `chemistry` int(3) NOT NULL,
  `biology` int(3) NOT NULL,
  `higherMath` int(3) NOT NULL,
  `geography` int(3) NOT NULL,
  `economics` int(3) NOT NULL,
  `civics` int(3) NOT NULL,
  `history` int(3) NOT NULL,
  `accounting` int(3) NOT NULL,
  `businessEntreprenureship` int(3) NOT NULL,
  `finance` int(3) NOT NULL,
  `generalScience` int(3) NOT NULL,
  `bangla1Student_id` varchar(11) NOT NULL,
  `bangla2Student_id` varchar(11) NOT NULL,
  `english1Student_id` varchar(11) NOT NULL,
  `english2Student_id` varchar(11) NOT NULL,
  `mathStudent_id` varchar(11) NOT NULL,
  `bgsStudent_id` varchar(11) NOT NULL,
  `scienceStudent_id` varchar(11) NOT NULL,
  `religionStudent_id` varchar(11) NOT NULL,
  `agricultureStudent_id` varchar(11) NOT NULL,
  `artsandcraftsStudent_id` varchar(11) NOT NULL,
  `ictStudent_id` varchar(11) NOT NULL,
  `wleStudent_id` varchar(11) NOT NULL,
  `physicalStudent_id` varchar(11) NOT NULL,
  `physicsStudent_id` varchar(11) NOT NULL,
  `chemistryStudent_id` varchar(11) NOT NULL,
  `biologyStudent_id` varchar(11) NOT NULL,
  `hmathStudent_id` varchar(11) NOT NULL,
  `geographyStudent_id` varchar(10) NOT NULL,
  `economicsStudent_id` varchar(11) NOT NULL,
  `civicsStudent_id` varchar(11) NOT NULL,
  `historyStudent_id` varchar(11) NOT NULL,
  `accountingStudent_id` varchar(11) NOT NULL,
  `businesseStudent_id` varchar(11) NOT NULL,
  `financeStudent_id` varchar(11) NOT NULL,
  `gscienceStudent_id` varchar(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `highest_mark`
--

INSERT INTO `highest_mark` (`id`, `examType`, `class`, `bangla1`, `bangla2`, `english1`, `english2`, `mathematics`, `bgs`, `science`, `religion`, `agriculture`, `artsandcrafts`, `ict`, `wle`, `physicalEducation`, `physics`, `chemistry`, `biology`, `higherMath`, `geography`, `economics`, `civics`, `history`, `accounting`, `businessEntreprenureship`, `finance`, `generalScience`, `bangla1Student_id`, `bangla2Student_id`, `english1Student_id`, `english2Student_id`, `mathStudent_id`, `bgsStudent_id`, `scienceStudent_id`, `religionStudent_id`, `agricultureStudent_id`, `artsandcraftsStudent_id`, `ictStudent_id`, `wleStudent_id`, `physicalStudent_id`, `physicsStudent_id`, `chemistryStudent_id`, `biologyStudent_id`, `hmathStudent_id`, `geographyStudent_id`, `economicsStudent_id`, `civicsStudent_id`, `historyStudent_id`, `accountingStudent_id`, `businesseStudent_id`, `financeStudent_id`, `gscienceStudent_id`) VALUES
(1, 'Monthly', 'Six', 89, 89, 87, 89, 96, 96, 95, 90, 89, 45, 45, 43, 42, 42, 45, 45, 46, 47, 47, 48, 49, 46, 43, 42, 43, 'S-111001', 'S-111002', 'S-111001', 'S-111002', 'S-111001', 'S-111001', 'S-111001', 'S-111001', 'S-111002', 'S-111002', 'S-111001', 'S-111004', 'S-111004', 'S-111004', 'S-111004', 'S-111004', 'S-111001', 'S-111001', 'S-111001', 'S-111001', 'S-111001', 'S-111004', 'S-111004', 'S-111004', 'S-111004'),
(2, 'Monthly', 'Six', 89, 89, 87, 89, 96, 96, 95, 90, 89, 45, 45, 43, 42, 42, 45, 45, 46, 47, 47, 48, 49, 46, 43, 42, 43, 'S-111001', 'S-111002', 'S-111001', 'S-111002', 'S-111001', 'S-111001', 'S-111001', 'S-111001', 'S-111002', 'S-111002', 'S-111001', 'S-111004', 'S-111004', 'S-111004', 'S-111004', 'S-111004', 'S-111001', 'S-111001', 'S-111001', 'S-111001', 'S-111001', 'S-111004', 'S-111004', 'S-111004', 'S-111004');

-- --------------------------------------------------------

--
-- Table structure for table `notices`
--

CREATE TABLE IF NOT EXISTS `notices` (
`id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `date` date NOT NULL,
  `level` varchar(20) NOT NULL,
  `status` varchar(5) NOT NULL DEFAULT 'Yes'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notices`
--

INSERT INTO `notices` (`id`, `title`, `description`, `date`, `level`, `status`) VALUES
(1, 'Exam From Fill', 'All Students come and fill their exam forms	All Students come and fill their exam forms. Unless they will not give further chance to fill up the form. As a result they will not get to sen in exam	All Students come and fill their exam forms	All Students come and fill their exam forms. Unless they will not give further chance to fill up the form. As a result they will not get to sen in exam	All Students come and fill their exam forms	All Students come and fill their exam forms. Unless they will not give further chance to fill up the form. As a result they will not get to sen in exam	All Students come and fill their exam forms	All Students come and fill their exam forms. Unless they will not give further chance to fill up the form. As a result they will not get to sen in exam	', '2017-03-05', 'General', 'Yes'),
(2, 'Attendance Report', 'All Employees collect their class wise attendance report', '2017-03-06', 'Employee', 'Yes'),
(3, 'Summer Vacation', 'Summer Vacation starts from June to 2nd week of July.', '2017-03-24', 'General', 'Yes'),
(6, 'Class Test', 'New Sunday Class Test Will be held', '2017-03-10', 'Student', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `predefined_marks`
--

CREATE TABLE IF NOT EXISTS `predefined_marks` (
`id` int(11) NOT NULL,
  `bangla1FullMark` int(3) NOT NULL,
  `bangla1PassMark` int(3) NOT NULL,
  `bangla2FullMark` int(3) NOT NULL,
  `bangla2PassMark` int(3) NOT NULL,
  `english1FullMark` int(3) NOT NULL,
  `english1PassMark` int(3) NOT NULL,
  `english2FullMark` int(3) NOT NULL,
  `english2PassMark` int(3) NOT NULL,
  `mathFullMark` int(3) NOT NULL,
  `mathPassMark` int(3) NOT NULL,
  `bgsFullMark` int(3) NOT NULL,
  `bgsPassMark` int(3) NOT NULL,
  `scienceFullMark` int(3) NOT NULL,
  `sciencePassMark` int(3) NOT NULL,
  `religionFullMark` int(3) NOT NULL,
  `religionPassMark` int(3) NOT NULL,
  `agricultureFullMark` int(3) NOT NULL,
  `agriculturePassMark` int(3) NOT NULL,
  `artsandcraftsFullMark` int(3) NOT NULL,
  `artsandcraftsPassMark` int(3) NOT NULL,
  `ictFullMark` int(3) NOT NULL,
  `ictPassMark` int(3) NOT NULL,
  `wleFullMark` int(3) NOT NULL,
  `wlePassMark` int(3) NOT NULL,
  `physicalFullMark` int(3) NOT NULL,
  `physicalPassMark` int(3) NOT NULL,
  `physicsFullMark` int(3) NOT NULL,
  `physicsPassMark` int(3) NOT NULL,
  `chemistryFullMark` int(3) NOT NULL,
  `chemistryPassMark` int(3) NOT NULL,
  `biologyFullMark` int(3) NOT NULL,
  `biologyPassMark` int(3) NOT NULL,
  `hmathFullMark` int(3) NOT NULL,
  `hmathPassMark` int(3) NOT NULL,
  `geographyFullMark` int(3) NOT NULL,
  `geographyPassMark` int(3) NOT NULL,
  `economicsFullMark` int(3) NOT NULL,
  `economicsPassMark` int(3) NOT NULL,
  `civicsFullMark` int(3) NOT NULL,
  `civicsPassMark` int(3) NOT NULL,
  `historyFullMark` int(3) NOT NULL,
  `historyPassMark` int(3) NOT NULL,
  `accountingFullMark` int(3) NOT NULL,
  `accountingPassMark` int(3) NOT NULL,
  `businesseFullMark` int(3) NOT NULL,
  `businessePassMark` int(3) NOT NULL,
  `financeFullMark` int(3) NOT NULL,
  `financePassMark` int(3) NOT NULL,
  `gscienceFullMark` int(3) NOT NULL,
  `gsciencePassMark` int(3) NOT NULL,
  `examType` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `predefined_marks`
--

INSERT INTO `predefined_marks` (`id`, `bangla1FullMark`, `bangla1PassMark`, `bangla2FullMark`, `bangla2PassMark`, `english1FullMark`, `english1PassMark`, `english2FullMark`, `english2PassMark`, `mathFullMark`, `mathPassMark`, `bgsFullMark`, `bgsPassMark`, `scienceFullMark`, `sciencePassMark`, `religionFullMark`, `religionPassMark`, `agricultureFullMark`, `agriculturePassMark`, `artsandcraftsFullMark`, `artsandcraftsPassMark`, `ictFullMark`, `ictPassMark`, `wleFullMark`, `wlePassMark`, `physicalFullMark`, `physicalPassMark`, `physicsFullMark`, `physicsPassMark`, `chemistryFullMark`, `chemistryPassMark`, `biologyFullMark`, `biologyPassMark`, `hmathFullMark`, `hmathPassMark`, `geographyFullMark`, `geographyPassMark`, `economicsFullMark`, `economicsPassMark`, `civicsFullMark`, `civicsPassMark`, `historyFullMark`, `historyPassMark`, `accountingFullMark`, `accountingPassMark`, `businesseFullMark`, `businessePassMark`, `financeFullMark`, `financePassMark`, `gscienceFullMark`, `gsciencePassMark`, `examType`) VALUES
(1, 100, 50, 100, 50, 100, 50, 100, 50, 100, 50, 100, 50, 100, 50, 100, 50, 100, 50, 50, 25, 50, 25, 50, 25, 50, 25, 100, 50, 100, 25, 100, 25, 100, 25, 100, 25, 100, 25, 100, 25, 100, 25, 100, 25, 100, 25, 100, 25, 100, 25, 'monthly');

-- --------------------------------------------------------

--
-- Table structure for table `science`
--

CREATE TABLE IF NOT EXISTS `science` (
`id` int(11) NOT NULL,
  `student_id` varchar(10) NOT NULL,
  `physics` int(3) NOT NULL,
  `chemistry` int(3) NOT NULL,
  `biology` int(3) NOT NULL,
  `higherMath` int(3) NOT NULL,
  `group` varchar(10) NOT NULL DEFAULT 'science'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `science`
--

INSERT INTO `science` (`id`, `student_id`, `physics`, `chemistry`, `biology`, `higherMath`, `group`) VALUES
(1, 'S-111002', 67, 67, 76, 67, 'science');

-- --------------------------------------------------------

--
-- Table structure for table `student_attendence`
--

CREATE TABLE IF NOT EXISTS `student_attendence` (
`id` int(11) NOT NULL,
  `student_id` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `class` varchar(20) NOT NULL,
  `attendence` varchar(10) NOT NULL DEFAULT 'absent'
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_attendence`
--

INSERT INTO `student_attendence` (`id`, `student_id`, `date`, `class`, `attendence`) VALUES
(1, 'S-111001', '2017-03-09', 'Class 9', 'absent'),
(2, 'L-123459', '2017-03-09', 'Class 9', 'absent'),
(3, '', '2017-03-09', 'Class 9', 'absent'),
(4, 'L-123457', '2017-03-09', 'Class 7', 'absent'),
(5, 'L-123458', '2017-03-09', 'Class 7', 'absent'),
(6, 'S-111001', '2017-03-10', 'Class 9', 'present'),
(7, 'L-123459', '2017-03-10', 'Class 9', 'present'),
(8, '', '2017-03-10', 'Class 9', 'present'),
(9, 'S-111001', '2017-03-10', 'Class 9', 'absent'),
(10, 'L-123459', '2017-03-10', 'Class 9', 'absent'),
(11, '', '2017-03-10', 'Class 9', 'absent'),
(12, 'S-111001', '2017-03-11', 'Class 9', 'present'),
(13, 'L-123459', '2017-03-11', 'Class 9', 'present'),
(14, '', '2017-03-11', 'Class 9', 'absent'),
(15, 'L-123457', '2017-03-10', 'Class 7', 'absent'),
(16, 'L-123458', '2017-03-10', 'Class 7', 'present'),
(19, 'L-123457', '2017-03-11', 'Class 7', 'present'),
(20, 'L-123458', '2017-03-11', 'Class 7', 'present'),
(21, 'S-111001', '2017-03-17', 'Class 9', 'present'),
(22, 'L-123459', '2017-03-17', 'Class 9', 'present'),
(23, '', '2017-03-17', 'Class 9', 'absent'),
(24, 'S-111001', '2017-03-10', 'Class 6', 'present'),
(25, 'S-111002', '2017-03-10', 'Class 6', 'present'),
(26, 'S-111003', '2017-03-10', 'Class 6', 'present'),
(27, 'S-111004', '2017-03-10', 'Class 6', 'present'),
(28, 'S-111001', '2017-03-24', 'Class 9', 'absent'),
(29, 'L-123459', '2017-03-24', 'Class 9', 'present'),
(30, '', '2017-03-24', 'Class 9', 'present'),
(31, 'S-111001', '2017-03-30', 'Class 9', 'present'),
(32, 'L-123459', '2017-03-30', 'Class 9', 'present'),
(33, '', '2017-03-30', 'Class 9', 'present'),
(34, 'S-111001', '2017-03-14', 'Class 6', 'present'),
(35, 'S-111002', '2017-03-14', 'Class 6', 'present'),
(36, 'S-111003', '2017-03-14', 'Class 6', 'present'),
(37, 'S-111004', '2017-03-14', 'Class 6', 'present');

-- --------------------------------------------------------

--
-- Table structure for table `student_info`
--

CREATE TABLE IF NOT EXISTS `student_info` (
`id` int(11) NOT NULL,
  `student_id` varchar(50) NOT NULL,
  `student_roll` int(10) NOT NULL,
  `student_section` varchar(5) NOT NULL,
  `apply_date` date NOT NULL,
  `student_name` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `birthday` date NOT NULL,
  `nationality` varchar(50) NOT NULL,
  `religion` varchar(20) NOT NULL,
  `blood_group` varchar(10) NOT NULL,
  `student_class` varchar(20) NOT NULL,
  `student_group` varchar(20) NOT NULL,
  `fathers_name` varchar(100) NOT NULL,
  `fathers_profession` varchar(100) NOT NULL,
  `fathers_contact` int(15) NOT NULL,
  `fathers_email` varchar(200) NOT NULL,
  `mothers_name` varchar(100) NOT NULL,
  `mothers_profession` varchar(100) NOT NULL,
  `mothers_contact` int(15) NOT NULL,
  `mothers_email` varchar(200) NOT NULL,
  `present_village` varchar(100) NOT NULL,
  `present_thana` varchar(50) NOT NULL,
  `present_upazilla` varchar(50) NOT NULL,
  `present_district` varchar(50) NOT NULL,
  `present_division` varchar(50) NOT NULL,
  `present_country` varchar(20) NOT NULL,
  `permanent_village` varchar(100) NOT NULL,
  `permanent_thana` varchar(50) NOT NULL,
  `permanent_upazilla` varchar(50) NOT NULL,
  `permanent_district` varchar(50) NOT NULL,
  `permanent_division` varchar(50) NOT NULL,
  `permanent_country` varchar(20) NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_info`
--

INSERT INTO `student_info` (`id`, `student_id`, `student_roll`, `student_section`, `apply_date`, `student_name`, `gender`, `birthday`, `nationality`, `religion`, `blood_group`, `student_class`, `student_group`, `fathers_name`, `fathers_profession`, `fathers_contact`, `fathers_email`, `mothers_name`, `mothers_profession`, `mothers_contact`, `mothers_email`, `present_village`, `present_thana`, `present_upazilla`, `present_district`, `present_division`, `present_country`, `permanent_village`, `permanent_thana`, `permanent_upazilla`, `permanent_district`, `permanent_division`, `permanent_country`, `file_name`, `password`) VALUES
(1, 'S-111001', 1, 'A', '0000-00-00', 'Sahed Sazzad', 'Male', '2017-02-24', 'Bangladeshi', 'Islam', 'A+', 'Class 9', 'Science', 'Nurul Islam', 'Businessman', 1917864523, 'sadaddid@yahoo.com', 'Sabina Yesmin', 'Housewife', 1817675645, 'saimarahaman@gmail.com', 'Panchkine', 'Raozan', 'Raozan', 'Chittagong', 'Chittagong', 'Bangladesh', 'Panchkine', 'Raozan', 'Raozan', 'Chittagong', 'Chittagong', 'Bangladesh', 'img9.jpg', 'b0baee9d279d34fa1dfd71aadb908c3f'),
(2, 'L-123457', 2, 'B', '0000-00-00', 'Yaser Iqbal', 'Male', '2017-02-15', 'Bangladeshi', 'Islam', 'O+', 'Class 7', '', 'Abdul Karim', 'Businessman', 1817894565, 'abdulkarim.ctg@yahoo.com', 'Amena Karim', 'Housewife', 1918786756, 'amena.karim@gmail.com', 'Muradpur', 'Panchlaish', 'Panchlaish', 'Chittagong', 'Chittagong', 'Bangladesh', 'Muradpur', 'Panchlaish', 'Panchlaish', 'Chittagong', 'Chittagong', 'Bangladesh', 'maxresdefault.jpg', '827ccb0eea8a706c4c34a16891f84e7b'),
(3, 'L-123458', 3, 'B', '0000-00-00', 'Abu Nayeem', 'Male', '2017-02-18', 'Bangladeshi', 'Islam', 'O+', 'Class 7', 'Arts', 'Nur Mohammad', 'Banker', 1918786754, 'nurmohammmad@gmail.com', 'Lipi', 'Housewife', 1817908788, 'lipi121@yahoo.com', 'Muradpur', 'Panchlaish', 'Panchlaish', 'Chittagong', 'Chittagong', 'Bangladesh', 'Chandpur', 'Chandpur', 'Chandpur', 'Chandpur', 'Chittagong', 'Bangladesh', 'Penguins.jpg', '6774b084b4e8c3189c507afa8a861a2d'),
(4, 'L-123459', 4, 'B', '0000-00-00', 'Rubel', 'Male', '2017-02-07', 'Bangladeshi', 'Islam', 'A+', 'Class 9', 'Science', 'Nurul Islam', 'Businessman', 1917897644, 'nurmohammmad@gmail.com', 'Samia Rahaman', 'Housewife', 191787565, 'saimarahaman@gmail.com', 'Panchkine', 'Raozan', 'Raozan', 'Chittagong', 'Chittagong', 'Bangladesh', 'Panchkine', 'Raozan', 'Raozan', 'Chittagong', 'Chittagong', 'Bangladesh', 'indian_villages_paintings_beach_nature_hut_street_agriculture_.jpg', ''),
(19, '', 0, '', '0000-00-00', 'Sahed Iqbal', 'Male', '2017-03-23', 'Bangladeshi', 'Islam', 'O+', 'Class 9', 'Arts', 'Zafar chy', 'Banker', 1911877777, 'nurmohammmad@gmail.com', 'Samia Rahaman', 'Housewife', 191188188, 'amena.karim@gmail.com', 'Muradpur', 'Panchlaish', 'Panchlaish', 'Chittagong', 'Chittagong', 'Bangladesh', 'Panchkine', 'Raozan', 'Raozan', 'Chittagong', 'Chittagong', 'Bangladesh', 'img9.jpg', ''),
(20, 'S-111001', 1, 'A', '0000-00-00', 'Mahmudul Hasan Sohag', 'Male', '2005-06-14', 'Bangladeshi', 'Islam', 'O+', 'Class 6', '', 'Nurul Islam', 'Businessman', 1817878897, 'nurulislam@yahoo.com', 'Sultana Begum', 'Housewife', 1817876566, 'sultana.ctg@gmail.com', 'Gumdondi', 'Boalkhali', 'Boalkhali', 'Chittagong', 'Chittagong', 'Bangladesh', 'Gumdondi', 'Boalkhali', 'Boalkhali', 'Chittagong', 'Chittagong', 'Bangladesh', 'img12.jpg', '827ccb0eea8a706c4c34a16891f84e7b'),
(21, 'S-111002', 2, 'A', '0000-00-00', 'Anisur Rahaman', 'Male', '2009-06-09', 'Bangladeshi', 'Islam', 'O-', 'Class 6', '', 'Zafar chy', 'Banker', 1917898989, 'zaforchowdhury@yahoo.com', 'Abida Khatun', 'Housewife', 1918878788, 'abida123@yahoo.com', 'Kalampati', 'Raozan', 'Raozan', 'Chittagong', 'Chittagong', 'Bangladesh', 'Kalampati', 'Raozan', 'Raozan', 'Chittagong', 'Chittagong', 'Bangladesh', 'img11.jpg', ''),
(22, 'S-111003', 3, 'A', '0000-00-00', 'Zabed Karim', 'Male', '2017-03-09', 'Bangladeshi', 'Islam', 'AB+', 'Class 6', '', 'Niaz Mahmud', 'Businessman', 1819898989, 'niaz0123huda@yahoo.com', 'Doli Zohur', 'Housewife', 191898993, 'zohuratdhaka@gmail.com', 'Mirpur 2', 'Mirpur', 'Gazipur', 'Dhaka', 'Dhaka', 'Bangladesh', 'Mirpur 2', 'Mirpur', 'Gazipur', 'Dhaka', 'Dhaka', 'Bangladesh', 'img7.jpg', ''),
(23, 'S-111004', 4, 'A', '0000-00-00', 'Atikur Rahman', 'Male', '0000-00-00', 'Bangladeshi', 'Islam', 'O-', 'Class 6', '', 'Hamidur Rahman', 'Police Inspector', 1918998878, 'rahman@gmail.com', 'Husne Ara Begum', 'Teacher', 1819786777, 'banu@hotmail.com', 'Farmgate', 'Panthapath', 'Panthapath', 'Dhaka', 'Dhaka', 'Bangladesh', 'Satkhira', 'Raipur', 'Laxmipur', 'Laxmipur', 'Chittagong', 'Bangladesh', 'img6.jpg', 'e10adc3949ba59abbe56e057f20f883e'),
(24, '', 0, '', '0000-00-00', '', '---Select ', '0000-00-00', '', '', '', '', '', '', '', 0, '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(25, '', 0, '', '0000-00-00', '', '---Select ', '0000-00-00', '', '', '', '', '', '', '', 0, '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(26, '', 0, '', '0000-00-00', '', '---Select ', '0000-00-00', '', '', '', '', '', '', '', 0, '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(27, '', 0, '', '0000-00-00', 'Limon', 'Male', '2017-03-14', 'Bangladeshi', 'Buddist', 'O+', 'Class 7', '', 'Bisu Barua', 'Businessman', 1918998988, 'bisu@yahoo.com', 'Lolita Borua', 'Housewife', 1819909900, 'lolita@gmail.com', 'Jobra', 'Hatazari', 'Hatazari', 'Chittagong', 'Chittagong', 'Bangladesh', 'Jobra', 'Hatazari', 'Hatazari', 'Chittagong', 'Chittagong', 'Bangladesh', '171782_185994668099908_100000681295465_502955_3502055_o.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_products`
--

CREATE TABLE IF NOT EXISTS `tbl_products` (
`product_id` int(11) NOT NULL,
  `product_name` varchar(35) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_products`
--

INSERT INTO `tbl_products` (`product_id`, `product_name`) VALUES
(1, 'Galaxy Jmax'),
(2, 'Killer Note5'),
(3, 'Asus ZenFone2'),
(4, 'Moto Xplay'),
(5, 'Lenovo Vibe k5 Plus'),
(6, 'Redme Note 3'),
(7, 'LeEco Le 2'),
(8, 'Apple iPhone 6S Plus');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_academic_info`
--

CREATE TABLE IF NOT EXISTS `teacher_academic_info` (
`id` int(11) NOT NULL,
  `teacher_id` varchar(20) NOT NULL,
  `ssc` varchar(10) NOT NULL,
  `ssc_institution` varchar(100) NOT NULL,
  `ssc_board` varchar(50) NOT NULL,
  `ssc_gpa` float NOT NULL,
  `ssc_passing_year` date NOT NULL,
  `hsc` varchar(20) NOT NULL,
  `hsc_institution` varchar(100) NOT NULL,
  `hsc_board` varchar(50) NOT NULL,
  `hsc_gpa` float NOT NULL,
  `hsc_passing_year` date NOT NULL,
  `honours` varchar(20) NOT NULL,
  `honours_institution` varchar(100) NOT NULL,
  `honours_board` varchar(50) NOT NULL,
  `honours_gpa` float NOT NULL,
  `honours_passing_year` date NOT NULL,
  `masters` varchar(20) NOT NULL,
  `master_institution` varchar(100) NOT NULL,
  `master_board` varchar(50) NOT NULL,
  `master_gpa` float NOT NULL,
  `master_passing_year` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_academic_info`
--

INSERT INTO `teacher_academic_info` (`id`, `teacher_id`, `ssc`, `ssc_institution`, `ssc_board`, `ssc_gpa`, `ssc_passing_year`, `hsc`, `hsc_institution`, `hsc_board`, `hsc_gpa`, `hsc_passing_year`, `honours`, `honours_institution`, `honours_board`, `honours_gpa`, `honours_passing_year`, `masters`, `master_institution`, `master_board`, `master_gpa`, `master_passing_year`) VALUES
(1, 'T001', 'SSC', 'Dewanpur S.K. Sen High School', 'Chittagong', 5, '2017-02-08', 'HSC', 'Imam Gazzali University College', 'Chittagong', 4.98, '2017-02-28', 'Honours', 'International Islamic University Chittagong', 'International Islamic University Chittagong', 4, '2017-02-10', 'Masters', 'International Islamic University Chittagong', 'International Islamic University Chittagong', 3.45, '2017-02-26'),
(2, 'T002', 'SSC', 'Chittagong Govt. High', 'Chiitagong', 5, '2017-02-07', 'HSC', 'Chittagong College', 'Chiitagong', 5, '2017-02-17', 'Honours', 'Chittagong University', 'Chittagong University', 4, '2017-02-20', 'Masters', 'Chittagong University', 'Chittagong University', 4, '2017-02-25'),
(3, 'T003', 'SSC', 'Dhaka Zilla School', 'Dhaka Board', 5, '2017-02-14', 'HSC', 'Dhaka College', 'Dhaka Board', 4.98, '2017-02-18', 'Honours', 'Dhaka University', 'Dhaka University', 4, '2017-02-23', 'Masters', 'Dhaka University', 'Dhaka University', 3.45, '2017-02-07'),
(4, 'T004', 'SSC', 'Gazipur Model School', 'Dhaka', 4.9, '2017-03-13', 'HSC', 'ABC College', 'Dhaka', 5, '2017-03-22', 'Honours', 'Dhaka University', 'Dhaka University', 4, '2017-03-31', 'Masters', 'Dhaka University', 'Dhaka University', 3.45, '2017-03-14'),
(5, 'T005', 'SSC', 'Dewanpur S.K. Sen High School', 'Chiitagong', 4.56, '2017-03-22', 'HSC', 'Dhaka College', 'Dhaka', 5, '2017-03-15', 'Honours', 'Dhaka University', 'Dhaka University', 4, '2017-03-14', 'Masters', 'Dhaka University', 'Dhaka University', 3.45, '2017-03-08'),
(6, 'T-111001', 'SSC', 'Ranirhat Govt. High School', 'Chittagong', 4.68, '2008-07-17', 'HSC', 'Chittagong Govt. College', 'Chittagong', 5, '2010-08-25', 'Honours', 'Chittagong University', 'Chittagong University', 3.9, '2014-12-17', 'Masters', 'Chittagong University', 'Chittagong', 4, '2015-12-14'),
(7, 'T-111002', 'SSC', 'Chittagong Govt. High', 'Chittagong', 5, '2006-07-13', 'HSC', 'Chittagong College', 'Chittagong', 5, '2008-11-20', 'Honours', 'Chittagong University', 'Chittagong University', 3.4, '2012-10-17', 'Masters', 'Chittagong University', 'Dhaka University', 3.5, '2013-11-28'),
(8, '', 'SSC', 'ABC High School', 'Chittagong', 5, '2017-03-22', 'HSC', 'ABC College', 'Chittagong', 5, '2017-03-09', 'Honours', 'Chittagong University', 'Chittagong University', 4, '2017-03-01', 'Masters', 'Chittagong University', 'Chittagong University', 4, '2017-03-29'),
(9, '', 'SSC', 'ABC High School', 'Chittagong', 5, '2017-03-22', 'HSC', 'ABC College', 'Chittagong', 5, '2017-03-09', 'Honours', 'Chittagong University', 'Chittagong University', 4, '2017-03-01', 'Masters', 'Chittagong University', 'Chittagong University', 4, '2017-03-29');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_attendence`
--

CREATE TABLE IF NOT EXISTS `teacher_attendence` (
`id` int(11) NOT NULL,
  `teacher_id` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `attendence` varchar(10) NOT NULL DEFAULT 'absent'
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_attendence`
--

INSERT INTO `teacher_attendence` (`id`, `teacher_id`, `date`, `attendence`) VALUES
(1, 'T001', '2017-03-12', 'present'),
(2, 'T002', '2017-03-12', 'present'),
(3, 'T003', '2017-03-12', 'absent'),
(4, 'T004', '2017-03-12', 'present'),
(5, 'T005', '2017-03-12', 'present'),
(6, 'T-111001', '2017-03-12', 'absent'),
(7, 'T-111002', '2017-03-12', 'present'),
(8, 'T001', '2017-03-13', 'present'),
(9, 'T002', '2017-03-13', 'present'),
(10, 'T003', '2017-03-13', 'present'),
(11, 'T004', '2017-03-13', 'present'),
(12, 'T005', '2017-03-13', 'present'),
(13, 'T-111001', '2017-03-13', 'present'),
(14, 'T-111002', '2017-03-13', 'present');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_info`
--

CREATE TABLE IF NOT EXISTS `teacher_info` (
`id` int(11) NOT NULL,
  `teacher_id` varchar(20) NOT NULL,
  `teacher_name` varchar(50) NOT NULL,
  `fathers_name` varchar(50) NOT NULL,
  `mothers_name` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `email` varchar(120) NOT NULL,
  `contact_no` int(15) NOT NULL,
  `birthday` date NOT NULL,
  `nationality` varchar(20) NOT NULL,
  `religion` varchar(20) NOT NULL,
  `blood_group` varchar(10) NOT NULL,
  `nid` int(11) NOT NULL,
  `present_village` varchar(100) NOT NULL,
  `present_thana` varchar(100) NOT NULL,
  `present_upazilla` varchar(100) NOT NULL,
  `present_district` varchar(100) NOT NULL,
  `present_division` varchar(100) NOT NULL,
  `present_country` varchar(100) NOT NULL,
  `permanent_village` varchar(100) NOT NULL,
  `permanent_thana` varchar(100) NOT NULL,
  `permanent_upazilla` varchar(100) NOT NULL,
  `permanent_district` varchar(100) NOT NULL,
  `permanent_division` varchar(100) NOT NULL,
  `permanent_country` varchar(100) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `joining_date` date NOT NULL,
  `subject` varchar(50) NOT NULL,
  `reference` varchar(200) NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_info`
--

INSERT INTO `teacher_info` (`id`, `teacher_id`, `teacher_name`, `fathers_name`, `mothers_name`, `gender`, `email`, `contact_no`, `birthday`, `nationality`, `religion`, `blood_group`, `nid`, `present_village`, `present_thana`, `present_upazilla`, `present_district`, `present_division`, `present_country`, `permanent_village`, `permanent_thana`, `permanent_upazilla`, `permanent_district`, `permanent_division`, `permanent_country`, `designation`, `joining_date`, `subject`, `reference`, `file_name`, `password`) VALUES
(1, 'T001', 'Rafiqul Islam', 'Habibur Rahaman', 'Sahanaj Akther', 'Male', 'rafiqulislam@yahoo.com', 1927814556, '1990-01-08', 'Bangladeshi', 'Islam', 'A+', 0, 'Savar', 'Savar', 'Gazipur', 'Dhaka', 'Dhaka', 'Bangladesh', 'Savar', 'Savar', 'Chandpur', 'Dhaka', 'Dhaka', 'Bangladesh', 'Class Teacher', '2017-02-24', 'Bangla', 'Abdul Kalam,Senior Teacher, Bangla', 'img0.jpg', 'b0baee9d279d34fa1dfd71aadb908c3f'),
(2, 'T002', 'Jabed Karim', 'Mahmudul Hasan', 'Sumaya Akter', 'Male', 'jabed.ctg.karim@gmail.com', 1816785645, '2017-02-01', 'Bangladeshi', 'Islam', 'O+', 0, '', '', '', '', '', '', '', '', '', '', '', '', 'Class Teacher', '2017-02-25', 'English', 'Ashraful Kabir', 'Hydrangeas.jpg', '827ccb0eea8a706c4c34a16891f84e7b'),
(3, 'T003', 'Ibrahim Khondokar', 'Shimul pathowary', 'Joynab Begum', 'Male', 'ms24hossain@gmail.com', 1918181818, '2017-02-28', 'Bangladeshi', 'Islam', 'O-', 0, '', '', '', '', '', '', '', '', '', '', '', '', 'Class Teacher', '2017-02-22', 'Mathematics', 'Kabir Uddin', 'Jellyfish.jpg', '6774b084b4e8c3189c507afa8a861a2d'),
(4, 'T004', 'Abdul Kaium', 'Motiur Rahaman', 'Joynab Bibi', 'Male', 'kaium.dhaka@gmail.com', 1918786765, '2017-03-16', 'Bangladeshi', 'Islam', 'AB+', 2147483647, 'Savar', 'Gazipur', 'Gazipur', 'Dhaka', 'Dhaka', 'Bangladesh', 'Savar', 'Gazipur', 'Gazipur', 'Dhaka', 'Dhaka', 'Bangladesh', 'Class Teacher', '2017-03-14', 'BGS', 'Hamidur Rahman', 'Chrysanthemum.jpg', ''),
(5, 'T005', 'Abdul Malek', 'Nurul Islam', 'Samia Rahaman', 'Male', 'c.o1@bitm.org.bd', 1987455567, '2017-03-17', 'Bangladeshi', 'Islam', 'A+', 2147483647, 'Muradpur', 'Panchlaish', 'Raozan', 'Chittagong', 'Chittagong', 'Bangladesh', 'Muradpur', 'Panchlaish', 'Panchlaish', 'Chittagong', 'Chittagong', 'Bangladesh', 'Class Teacher', '2017-03-15', 'BGS', 'Abul Khair,Senior Teacher, English', 'Penguins.jpg', ''),
(6, 'T-111001', 'Abdur Rashid', 'Omar Faruk', 'Suraiya Tasnim', 'Male', 'rashid344@gmail.com', 1717676677, '1995-08-16', 'Bangladeshi', 'Islam', 'O+', 2147483647, 'Kawkhali', 'Ranirhat', 'Ranirhat', 'Rangamati', 'Chittagong', 'Bangladesh', 'Kawkhali', 'Ranirhat', 'Ranirhat', 'Rangamati', 'Chittagong', 'Bangladesh', 'Class Teacher', '2017-03-10', 'Science', 'Kader Khan', 'img5.jpg', 'd41d8cd98f00b204e9800998ecf8427e'),
(7, 'T-111002', 'Zahid Hasan Sakib', 'Afzal Sharif', 'Ayesha Khatun', 'Male', 'hasan.zahid@yahoo.com', 1817675655, '1990-12-16', 'Bangladeshi', 'Islam', 'O-', 2147483647, 'Gohira', 'Raozan', 'Raozan', 'Chittagong', 'Chittagong', 'Bangladesh', 'Gohira', 'Raozan', 'Raozan', 'Chittagong', 'Chittagong', 'Bangladesh', 'Coordinator', '2017-03-16', 'All', 'Hafiz Hasan', 'img1.jpg', 'd41d8cd98f00b204e9800998ecf8427e'),
(8, '', 'Abu Zayed hasnain Masum', 'Kium Chowdhury', 'Rajia Khanam', 'Male', 'tushar.chowdhury@gmail.com', 1819987888, '2017-03-09', 'Bangladeshi', 'Islam', 'O+', 2147483647, 'Savar', 'Gazipur', 'Gazipur', 'Dhaka', 'Dhaka', 'Bangladesh', 'Muradpur', 'Panchlaish', 'Panchlaish', 'Chittagong', 'Chittagong', 'Bangladesh', 'Instructor', '2017-03-16', 'PHP', 'BITM', '2013_Ind39.jpg', ''),
(9, '', 'Abu Zayed hasnain Masum', 'Kium Chowdhury', 'Rajia Khanam', 'Male', 'tushar.chowdhury@gmail.com', 1819987888, '2017-03-09', 'Bangladeshi', 'Islam', 'O+', 2147483647, 'Savar', 'Gazipur', 'Gazipur', 'Dhaka', 'Dhaka', 'Bangladesh', 'Muradpur', 'Panchlaish', 'Panchlaish', 'Chittagong', 'Chittagong', 'Bangladesh', 'Instructor', '2017-03-16', 'PHP', 'BITM', '2013_Ind39.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_upload`
--

CREATE TABLE IF NOT EXISTS `teacher_upload` (
`id` int(11) NOT NULL,
  `teacher_id` varchar(20) NOT NULL,
  `description` varchar(500) NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher_upload`
--

INSERT INTO `teacher_upload` (`id`, `teacher_id`, `description`, `file_name`, `status`) VALUES
(1, 'T001', 'Mark Sheet', 'admitest-notice-2015-2016.doc', 1),
(6, 'T002', 'Mark Sheet 2', '01simple (6).xlsx', 0),
(9, 'T003', 'Lecture', 'Basic Syntax, Variable.docx', 0),
(32, 'T001', 'Mark Sheet 2', 'Admission-Reschedule_05-12-2016.pdf', 1),
(33, 'T001', 'Mark Sheet 3', '119-2014-04-09-Pride and Prejudice.pdf', 1),
(34, 'T001', 'Test File', '119-2014-04-09-Pride and Prejudice.pdf', 1),
(35, 'T001', 'Mark Sheet 5', '119-2014-04-09-Pride and Prejudice.pdf', 1),
(36, 'T001', 'Mark Sheet 4', 'Admission-Reschedule_05-12-2016.pdf', 1),
(37, 'T001', 'Mark Sheet', '13138854_1176291539072452_5224459088257260612_n.jpg', 1),
(38, 'T001', 'Mark Sheet', '119-2014-04-09-Pride and Prejudice.pdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `first_name` varchar(111) NOT NULL,
  `last_name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(111) NOT NULL,
  `phone` varchar(111) NOT NULL,
  `address` varchar(333) NOT NULL,
  `email_verified` varchar(111) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `password`, `phone`, `address`, `email_verified`) VALUES
(6, 'Mohammad ', 'Rubel', 'rubelwd@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '01815816466', 'Chittagong', 'Yes');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `arts`
--
ALTER TABLE `arts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commerce`
--
ALTER TABLE `commerce`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_info`
--
ALTER TABLE `employee_info`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general`
--
ALTER TABLE `general`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_2`
--
ALTER TABLE `general_2`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `highest_mark`
--
ALTER TABLE `highest_mark`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notices`
--
ALTER TABLE `notices`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `predefined_marks`
--
ALTER TABLE `predefined_marks`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `science`
--
ALTER TABLE `science`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_attendence`
--
ALTER TABLE `student_attendence`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_info`
--
ALTER TABLE `student_info`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_products`
--
ALTER TABLE `tbl_products`
 ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `teacher_academic_info`
--
ALTER TABLE `teacher_academic_info`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_attendence`
--
ALTER TABLE `teacher_attendence`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_info`
--
ALTER TABLE `teacher_info`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_upload`
--
ALTER TABLE `teacher_upload`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `arts`
--
ALTER TABLE `arts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `commerce`
--
ALTER TABLE `commerce`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `employee_info`
--
ALTER TABLE `employee_info`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `general`
--
ALTER TABLE `general`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `general_2`
--
ALTER TABLE `general_2`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `highest_mark`
--
ALTER TABLE `highest_mark`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `notices`
--
ALTER TABLE `notices`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `predefined_marks`
--
ALTER TABLE `predefined_marks`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `science`
--
ALTER TABLE `science`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `student_attendence`
--
ALTER TABLE `student_attendence`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `student_info`
--
ALTER TABLE `student_info`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `tbl_products`
--
ALTER TABLE `tbl_products`
MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `teacher_academic_info`
--
ALTER TABLE `teacher_academic_info`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `teacher_attendence`
--
ALTER TABLE `teacher_attendence`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `teacher_info`
--
ALTER TABLE `teacher_info`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `teacher_upload`
--
ALTER TABLE `teacher_upload`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
